import EventEmitter from "eventemitter3";
import image from "../images/planet.svg";

export default class Application extends EventEmitter {

  static planets = [];
  static get events() {
    return {
      READY: "ready",
    };
  }

  constructor() {
    super();

    const result = this._load();

    this._create(result);




    this.emit(Application.events.READY);
  }
  _startLoading() { }
  _stopLoading() { }

  async _create(result) {

    await result.then((data) => {
      const innerResult = data.results;
      innerResult && innerResult.forEach(element => {
        const box = document.createElement("div");
        box.innerHTML = this._render({
          name: element.name,
          terrain: element.terrain,
          population: element.population,
        });
        document.body.querySelector(".main").appendChild(box);
      });


    });

  }

  async _load(url) {
    const API_URL = url || 'https://swapi.boom.dev/api/planets';
    return await fetch(API_URL)
      .then(res => res.json())
    // .then((res) => {
    //   if (res.next != null) {
    //     planets += res.results;
    //   }
    // })
  }

  _render({ name, terrain, population }) {
    return `
<article class="media">
  <div class="media-left">
    <figure class="image is-64x64">
      <img src="${image}" alt="planet">
    </figure>
  </div>
  <div class="media-content">
    <div class="content">
    <h4>${name}</h4>
      <p>
        <span class="tag">${terrain}</span> <span class="tag">${population}</span>
        <br>
      </p>
    </div>
  </div>
</article>
    `;
  }
}
